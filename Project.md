# Projet BSN2 OSAR02 

## Introduction

Vous allez devoir créer une application Web développée côté client avec React en Javascript, ainsi qu'une API node.js express.  
Cette application permettra à l'utilisateur d'organiser ses médias afin d'avoir une idée de ce qu'il a déjà visionné ou de ce qu'il voudrait visionner plus tard. 

Les points suivant seront abordés :

- Conception
- Composants *fonctionnels* en React
- Gestion des états et flux de données
- Gestion de routes React
- Mise en place d'un state management avec Redux Toolkit
- Développement d'une API
- Consommation d'une API
- Variables d'environements
- Connexion à une base de données postgres
- Requêtes SQL
- Utilisation d'un ORM (en cas de bon avancement du projet)

## Notation

La notation se basera sur :

- La propreté du code (indentation, respect des bonnes pratiques)
- Utilisation de commentaires pertinents dans le code quand jugé nécessaire à la compréhension du code
- Nommage des variables et des fonctions aidant à la compréhension du code (**Français OU Anglais**)
- Respect des conditions de rendu : projet sur GitLab, mon adresse en reporter et dernier commit avant le **15/04/22 23h59**. 
- Respect des consignes du projet 
- Fonctionnement des étapes demandés
- Utilisation des opérateurs JavaScript vu au premier semestre
- un README avec :  
    * votre nom, votre prénom
    * les étapes pour compiler votre projet :  **serveur (inclus les explications sur les variables d'environnement) ET client**.
    * les étapes non réalisées ou réalisées de manière approximatives avec les explications des difficultées rencontrées

## 1. Mettre en place un serveur express

- Créez un nouveau dossier *Project_Media_VOTRENOM*, placez vous à l'intérieur puis créez un dossier *server*.

 ### ⚠️ A partir d'ici toutes commandes et manipulations sont à éxécuter dans le dossier `/server`

- Exécutez la commande suivante:  `npm init -y`

Cette commande va nous permettre de générer le package.json qui contiendra les modules et dépendances nécessaires.

- Afin de mettre en place votre serveur express il vous faut installer [express](https://www.npmjs.com/package/express)

- Créez maintenant un fichier  server.js à l'intérieur duquel vous copierez les lignes suivantes : 

```js
const express = require('express')
const app = express()
const port = 5000

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
```

Il s'agit du minimum nécessaire pour faire tourner un server express sur votre machine.    

- Dans le package.json ajoutez-y le script suivant : `"start": "node server.js" ` 

Afin de maintenant le lancer vous pouvez éxécuter la commande: `npm run start`

Si dans la console de votre IDE vous voyez la phrase 'Example app listening on port 5000' vous pouvez continuer. 


## 2. Premier endpoint de l'API 

Vous allez maintenant créer votre premier endpoint. Sur votre premier projet vous avez appelé un endpoint qui vous fournissait des informations météorologiques, maintenant vous allez créer votre propre endpoint qui renverra la donnée demandé.

- Lisez la [documentation](https://expressjs.com/en/starter/basic-routing.html) afin de faire une route qu'on pourra appeler via le verbe HTTP GET.

- Vous renverrez sur la route `/test`  l'objet suivant : 
``` json
{
  prenom: 'votreprenom', 
  nom: 'votrenom'
} 
```

- ⚠️ Une fois la route ajoutée relancez votre serveur pour prendre en compte vos dernières modifications. 

- Afin de vérifier si votre endpoint fonctionne vous allez installer [Insomnia](https://insomnia.rest/) (Free version), on va se servir de la partie Client API qui va nous permettre de consommer des endpoints.
Notez que puisqu'il s'agit d'une requête GET vous pourriez aussi bien taper l'url dans votre navigateur cela fonctionnerait.

- Une fois Insomnia installé, créez une nouvelle collection avec le nom de votre choix, puis créez une première requête en sélectionnant le verbe adéquat
et nommez la "Test". 
Cliquez dessus (elle doit apparaitre dans la liste à gauche) et adaptez l'url pour appeler l'endpoint sur votre serveur.   
Cela devrait donner quelque chose comme: http://localhost:5000/test


En appuyant sur "Send" vous devez voir à droite l'objet {prenom: 'votreprenom', nom: 'votrenom'}, si c'est le cas vous pouvez continuer.


## 3. Nodemon

Dans l'étape 2 vous avez du redémarrer votre serveur afin que vos modifications soient prises en compte ce qui peut vite rendre la tâche pénible.
Pour contrer à cela nous allons utiliser le package Nodemon qui détecte automatiquement les changements sur votre serveur et redémarre le processus tout seul. 

- Installez [nodemon](https://www.npmjs.com/package/nodemon).
- Une fois fait dans votre package.json ajoutez-y le script suivant : `"start:dev": "nodemon server.js"`.  
- Coupez votre serveur et lancez la commande  ` npm run start:dev `
- Essayez de modifier votre route /test en modifiant un peu la donnée renvoyée puis sauvegardez.   

Si les modifications ont été prises en compte sans que vous n'ayez à relancer le serveur alors vous pouvez continuer. 

## 4. Environnement pour la base de donnée

Pour le projet nous allons utilisé une base de donnée SQL Postgresql.

### ⚠️ Mise à jour du sujet (03/03/2022) :  
(Puisque le port semble bloqué vous allez utiliser votre propre bd postgres installée en local). 

- Téléchargez le dernier installateur du serveur [postgres](https://www.postgresql.org/download/windows/), à priori l'installateur propose tout seul d'installer PgAdmin4 par la même occasion qui est lui un client postgres. Vous allez vous en servir juste après. Le mot de passe que vous choisirez sera celui qui servira à la connexion à la BD et que vous devrez renseigner dans vos variables d'environnements. 

- Ouvrez PgAdmin4, dans les serveurs à gauche si vous voyez un serveur comme PostgresSQL 14 c'est celui créé par défaut, partez de celui-ci qui renseigne déjà toutes les bonnes informations. 

- Déroulez les sous-menu de ce serveur jusqu'à atterir sur `Tables`, faites clique droit puis `Query Tool`.
Dans la console qui s'est ouverte, copiez-y le script suivant : [medias-table](./medias.sql), puis exécutez le.

- N'hésitez pas à refresh (clique-droit - refresh) le serveur et vérifiez que la table movies ainsi que les datas ont bien été insérées

Si tout s'est bien déroulé continuez : 


- Installez le [package](https://www.npmjs.com/package/pg) qui sera notre client pour nous connecter à la DB depuis notre serveur express.

- Créez un fichier .gitignore dans lequel vous copierez ceci :
```yml
# Node artificat files
/node_modules

# Environment variable file
.env 
```

- Nous allons maintenant créer un fichier `.env.example` qui sera commit sur votre répertoire git. Il permettra à tout développeur de connaitre le besoin en terme de variable d'environnement sur votre projet. Seul `.env` sera rempli avec les vraies valeurs et ne sera pas commité (grâce au `.gitignore`) afin de ne pas divulguer des informations confidentielles :
```yml
DB_USER=
DB_PASSWORD=
DB_HOST=
DB_DATABASE=
DB_PORT=
```
- Créez le fichier `.env` et remplissez le avec les valeurs de votre BD en locale.
- Créez un nouveau projet *Project_Media_VOTRENOM* sur votre compte Gitlab dans lequel vous pusherez le contenu de votre projet actuel. Vérifiez qu'il n'y ait ni de dossiers nodes_modules, ni de .env. Au passage prenez le temps de m'ajouter en reporter sur votre projet. 

- Installez le paquet [dotenv](https://www.npmjs.com/package/dotenv) qui nous permettra d'ajouter nos variables d'environnement.
- Dans votre fichier `server.js`, ajoutez de quoi charger vos variables d'environnement (voir doc du paquet [dotenv](https://www.npmjs.com/package/dotenv)), faites un `console.log(process.env)` et vérifiez que vous y trouvé vos nouvelles variables, vous pouvez aussi bien log directement `process.env.DB_USER` etc .. 

Si toutes les étapes sont validées vous pouvez continuer.


## 5. Connexion et requête à la base de donnée

Vous allez maintenant procéder à la connexion sur la base de données. Le paquet `node-postgres` que vous avez installé ultérieurement permet d'utiliser ce qu'on appelle un `Client` ou un `Pool`. Dans le projet vous vous servirez du `Client` car notre type d'application ne nécessitera qu'une seule connexion au client. 

Voici la [documentation officielle](https://node-postgres.com/). 

- Créez un fichier `db-config.js` qui exportera l'objet DB_CONFIG suivant :
```js
const DB_CONFIG = {
    user: process.env.DB_USER,
    host:  process.env.DB_HOST,
    database: process.env.DB_DATABASE,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    ssl: { rejectUnauthorized: false } // Option à enlever en local.
}

module.exports = { DB_CONFIG }
```

- Importez ce module dans le fichier `server.js` et passez l'objet `DB_CONFIG` en paramètre d'une nouvelle instance de `Client`. 

- Faites un endpoint GET dont le path sera `/series`. Cet endpoint retournera toutes les séries de la table `medias` en utilisant une `client.query`.

- Faites de même sur le path `/movies` pour les films. 

Vérifiez que les endpoints vous renvoient bien la donnée demandée avant de passer à la suite.

## 6. Mise en place d'une application React

- Placez vous à l'extérieur du dossier `/server` et [initialisez](https://fr.reactjs.org/docs/create-a-new-react-app.html) une nouvelle application React qui s'appellera `client`.
 ### ⚠️ A partir d'ici toutes les commandes sont à éxécuter dans le dossier `/client`

 Contrairement au 1er semestre, ce sera à vous de faire la conception afin de découper votre code au mieux entre composants réutilisables et vues. 
 
**Le cahier des charges est le suivant :** 

Pour simplifier on parlera de **médias** lorsqu'on souhaitera parler de séries et de films en même temps. 

Le client souhaite une médiathèque qui lui permettra de gérer une liste de médias :
- La médiathèque possèdera une vue par type (séries / films). 
- Une barre de navigation permettant de switcher entre séries ou films
- Sur chaque vue on y retrouvera une liste de tous les médias, une liste des médias en cours de visionnage, une liste des médias à regarder, une liste des médias terminés. 
- Un média est un élément d'une liste. Un média est composé de : son titre, son année de sortie et son affiche.   
Il possèdera également "une action" permettant de déplacer un élément dans une des listes mentionnées au dessus.  
- La liste de tous les médias devra toujours contenir l'intégralité des films ou séries requêtés. 
Déplacer un élément dans une des trois autres listes ne devra pas les retirer. A contrario si par exemple un élément de liste passe de la liste "en cours de visionnage" à "terminer" l'élément devra être évidemment retiré de "en cours de visionnage". 
Ces listes seront "sauvegardées" via du state management persistant grâce à Redux Toolkit (à voir dans les prochaines étapes).  



**Indications :**

- Reprenez à minima la structure `views` et `components` comme au premier semestre.
- Vous aurez un fichier `services.js` qui regroupera vos appels HTTP à votre API.   
Les appels à l'API dans vos composants utiliseront donc ces fonctions.
- La route principale (`/`) redirigera sur la route `/series` et vous aurez une autre route `/movies`
- Vous utiliserez des composants fonctionnels avec des hooks et non des classes.
- N'hésitez pas à faire une maquette rapidement sur papier pour vous aider à savoir comment vous allez découper vos composants.
- N'hésitez pas à utiliser [mui](https://mui.com/components/) pour vous simplifier la tâche en terme d'affichage / design.


Considérez cette étape terminée dès que :
- Votre application permet de naviguer entre les deux vues via la barre de navigation.
- Chaque vue affiche la première liste qui contient tout selon son type de médias (séries/film)
- Chaque élément de la liste possède titre / année / image.
 
## 7. Redux Toolkit 

- Installez les librairies nécessaires à l'utilisation de Redux Toolkit dans votre application :  
 [react-redux](https://www.npmjs.com/package/react-redux)  
 [@reduxjs/toolkit](https://www.npmjs.com/package/@reduxjs/toolkit)


 - Créez un [slice](https://redux-toolkit.js.org/api/createslice) qui contiendra votre initialState : càd les 6 listes dont vous aurez besoin (3 pour les séries, 3 pour les films). 
 - Le slice contiendra également les reducers qui permettront de déplacer un media dans une des listes. N'oubliez pas les contraintes sur les déplacements mentionnées dans la partie 6. Pensez à exporter ces reducers en tant qu'actions du slice pour pouvoir les utiliser facilement dans un [dispatch](https://react-redux.js.org/api/hooks#usedispatch). 
 - Une fois le slice créé n'oubliez pas de l'importer dans la [configuration](https://redux-toolkit.js.org/api/configureStore#basic-example) de votre store. 
 - N'oubliez pas de fournir le store à votre application et de le rendre [persistant](https://www.npmjs.com/package/redux-persist).
 - Ajoutez à vos vues (correspondant à Séries/Films) les autres listes intialisées avec la valeur des listes correspondantes dans votre initialState de votre slice. Utilisez pour cela le hook [UseSelector](https://react-redux.js.org/api/hooks#useselector).
 - Enfin ajoutez une action sur chacun de vos medias permettant de les déplacer dans ces listes grâces à vos reducers. Cela peut être un bouton avec un modal, plusieurs boutons ... etc. 

 *Tips:*   
 - Je recommande vivement cette [vidéo](https://www.youtube.com/watch?v=1lvnT2oE0_4&ab_channel=LiorCHAMLA) pour une meilleur compréhension.
 - L'extension Redux DevTools vous permettra de voir les exécutions des reducers sur les states. Pour [Chrome](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=fr) ou pour [Firefox](https://addons.mozilla.org/fr/firefox/addon/reduxdevtools/).


## 8. Ajouter un Média

- Développez un nouvel endpoint sur votre api permettant d'ajouter un média sur le path `/media`.   
En cas de succès la requête renverra un status `204` ou [l'id du media](https://stackoverflow.com/a/58526619) ajouté selon votre besoin. Dans le cas contraire `500` avec le message 'Error during the Request'. 

- Sur votre client développez la possibilité d'ajouter une nouvelle série ou un nouveau film. Vous pouvez soit ajouter un bouton sur les deux vues (séries / films) et ouvrir un formulaire sur un modal, soit directement un formulaire sur une nouvelle vue. Le choix est libre, cependant le formulaire que vous utiliserez sera biensûr unique. L'id étant auto incrémenté en base de donnée il ne devra pas apparaître dans le formulaire. 

- Pour la gestion du formulaire vous utiliserez [formik](https://formik.org/docs/overview) et plus précisément son [hook](https://formik.org/docs/api/useFormik). La gestion d'erreur doit être réalisé, tous les champs required ...etc.  Ne vous embêtez pas pour le champ concernant le poster, il s'agira juste d'une string puisqu'en BD on a enregistré uniquement une url vers une image.   

- Si l'ajout échoue cela doit être visible pour l'utilisateur. De même si l'ajout est un succès. Vous pouvez utilisez des [toasts](https://www.npmjs.com/package/react-toastify) ou autre.
De plus, l'utilisateur ne doit pas avoir à rafraichir la vue pour faire apparaître le nouveau média. Il vous sera sûrement utile que votre endpoint [renvoie l'id](https://stackoverflow.com/a/58526619) du media ajouté afin de mettre à jour votre state si vous procédez de la sorte. 

## 9. Supprimer un Média

- Développez un nouvel endpoint sur votre api permettant de supprimer un media sur le path `/media/:idMedia`.   
En cas de succès la requête renverra un status `204` dans le cas contraire `500` avec le message 'Error during the Request'. 

- Sur votre client développez la possibilité de supprimer une série ou un film, il devra y'avoir à minima une demande de confirmation. Si un média est supprimé, il doit être supprimé de toutes les listes.

- Si la supression échoue cela doit être visible pour l'utilisateur. De même si la suppression est un succès. L'utilisateur ne doit pas avoir à rafraichir la vue pour voir cette supression.
