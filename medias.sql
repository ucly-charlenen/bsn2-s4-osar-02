SET client_encoding = 'UTF8';


CREATE TYPE media_type as enum ('series', 'movies');


CREATE TABLE public.medias (
    "idMedia" bigint NOT NULL,
    title character varying NOT NULL,
    year integer NOT NULL,
    poster character varying NOT NULL,
    type public.media_type NOT NULL
);


CREATE SEQUENCE public."medias_idMedia_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public."medias_idMedia_seq" OWNED BY public.medias."idMedia";
ALTER TABLE ONLY public.medias ALTER COLUMN "idMedia" SET DEFAULT nextval('public."medias_idMedia_seq"'::regclass);





INSERT INTO public.medias ("idMedia", title, year, poster, type) VALUES (1, 'Lucifer', 2016, 'https://fr.web.img6.acsta.net/pictures/19/05/03/09/51/0733619.jpg', 'series');
INSERT INTO public.medias ("idMedia", title, year, poster, type) VALUES (2, 'Game of Thrones', 2011, 'https://image.posterlounge.fr/images/l/1901278.jpg', 'series');
INSERT INTO public.medias ("idMedia", title, year, poster, type) VALUES (3, 'Chicago Police Department', 2014, 'https://fr.web.img6.acsta.net/pictures/21/09/17/13/08/1711509.jpg', 'series');
INSERT INTO public.medias ("idMedia", title, year, poster, type) VALUES (4, 'Las Chicas Del Cable', 2017, 'https://fr.web.img3.acsta.net/r_1280_720/newsv7/17/03/03/12/00/3719110.jpg', 'series');
INSERT INTO public.medias ("idMedia", title, year, poster, type) VALUES (5, 'Dune', 2021, 'https://fr.web.img6.acsta.net/pictures/21/08/10/12/20/4633954.jpg', 'movies');
INSERT INTO public.medias ("idMedia", title, year, poster, type) VALUES (6, 'Spider-Man No Way Home', 2021, 'https://www.disneyphile.fr/wp-content/uploads/2021/11/spider-man-no-way-home-affiche-2.jpg', 'movies');
INSERT INTO public.medias ("idMedia", title, year, poster, type) VALUES (7, 'Harry Potter and the Philosopher''s Stone', 2001, 'https://m.media-amazon.com/images/I/815v2OuIHXL._AC_SL1500_.jpg', 'movies');
INSERT INTO public.medias ("idMedia", title, year, poster, type) VALUES (8, 'Warcraft The Beginning', 2016, 'https://www.next-stage.fr/wp-content/uploads/2016/03/Warcraft-Film-Affiche-internationale.jpg', 'movies');
INSERT INTO public.medias ("idMedia", title, year, poster, type) VALUES (9, 'Bohemian Rhapsody', 2018, 'https://www.avoir-alire.com/IMG/arton39153.jpg', 'movies');
INSERT INTO public.medias ("idMedia", title, year, poster, type) VALUES (10, 'The Witcher', 2019, 'https://fr.web.img6.acsta.net/pictures/19/12/12/12/13/2421997.jpg', 'series');
INSERT INTO public.medias ("idMedia", title, year, poster, type) VALUES (11, 'You', 2018, 'https://fr.web.img6.acsta.net/pictures/21/09/23/12/42/1352482.jpg', 'series');
INSERT INTO public.medias ("idMedia", title, year, poster, type) VALUES (12, 'La Casa De Papel', 2017, 'https://www.ecranlarge.com/media/cache/1600x1200/uploads/image/001/407/la-casa-de-papel-affiche-francaise-volume-2-1407243.jpg', 'series');
INSERT INTO public.medias ("idMedia", title, year, poster, type) VALUES (13, 'Breaking Bad', 2008, 'https://fr.web.img5.acsta.net/pictures/19/06/18/12/11/3956503.jpg', 'series');



SELECT pg_catalog.setval('public."medias_idMedia_seq"', 13, true);


ALTER TABLE ONLY public.medias
    ADD CONSTRAINT medias_pk PRIMARY KEY ("idMedia");

CREATE UNIQUE INDEX medias_idmedia_uindex ON public.medias USING btree ("idMedia");
